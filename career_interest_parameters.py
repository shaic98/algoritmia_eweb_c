class CareerInterestParameters(object):

    def __init__(self, city_size_importance, has_coast_importance, cultural_score_importance,
                 gastronomic_score_importance, leisure_score_importance, job_offer_score_importance, average_rental_price_importance,
                 cut_off_grade_importance):

        self.__city_size_importance = city_size_importance
        self.__has_coast_importance = has_coast_importance
        self.__cultural_score_importance = cultural_score_importance
        self.__gastronomic_score_importance = gastronomic_score_importance
        self.__leisure_score_importance = leisure_score_importance
        self.__job_offer_score_importance = job_offer_score_importance
        self.__average_rental_price_importance = average_rental_price_importance
        self.__cut_off_grade_importance = cut_off_grade_importance

    def get_city_size_importance(self):
        return self.__city_size_importance

    def get_has_coast_importance(self):
        return self.__has_coast_importance

    def get_cultural_score_importance(self):
        return self.__cultural_score_importance

    def get_gastronomic_score_importance(self):
        return self.__gastronomic_score_importance

    def get_leisure_score_importance(self):
        return self.__leisure_score_importance

    def get_job_offer_score_importance(self):
        return self.__job_offer_score_importance

    def get_average_rental_price_importance(self):
        return self.__average_rental_price_importance

    def get_cut_off_grade_importancee(self):
        return self.__cut_off_grade_importance
