from career_interest_parameters import CareerInterestParameters
from degree_relevance import DegreeRelevance

#mock_career_params = CareerInterestParameters(0, 2, 2, 3, 0, 1, 2, 3)

#mock_degree_relevance = DegreeRelevance(4, "201-300", mock_career_params, 308, False, 4.5, 3.2, 5, 2, 8.2, 13)

mock_career_params = CareerInterestParameters(0, 2, 2, 3, 0, 1, 2, 3)

mock_degree_relevance = DegreeRelevance(5, "50", mock_career_params, 502, True, 5, 5, 5, 5, 1, 14)

print("Degree relevance: ", mock_degree_relevance.relevance_calculation())

print("Degree relevance: ", mock_degree_relevance.get_job_offer_score())