import math

class DegreeRelevance(object):

    def __init__(self, average_score_univ_opinions, shanghai_ranking_position, parameter_relevance, city_size,
                 has_coast, cultural_score, gastronomic_score, leisure_score, job_offer_score, average_rental_price,
                 cut_off_grade):
        # Fixed inputs
        self.__average_score_univ_opinions = round(average_score_univ_opinions)
        self.__shanghai_ranking_position = self.shanghai_ranking_position_scaling(shanghai_ranking_position)

        # Optional inputs
        self.__parameter_relevance = parameter_relevance
        self.__scaled_city_size = self.city_size_scaling(city_size)
        self.__scaled_has_coast = self.boolean_scaling(has_coast)
        self.__cultural_score = round(cultural_score)
        self.__gastronomic_score = round(gastronomic_score)
        self.__leisure_score = round(leisure_score)
        self.__job_offer_score = round(job_offer_score)
        self.__scaled_average_rental_price = self.rental_price_scaling(average_rental_price)
        self.__scaled_cut_off_grade = self.cut_off_grade_scaling(cut_off_grade)

    @staticmethod
    def shanghai_ranking_position_scaling(shanghai_ranking_position):

        if "-" in shanghai_ranking_position:
            rank_edges = shanghai_ranking_position.split("-", 2)
            aux_shanghai_ranking_position = int(rank_edges[0])
        else:
            aux_shanghai_ranking_position = int(shanghai_ranking_position)

        if aux_shanghai_ranking_position < 100:
            scaled_shanghai_ranking_position = 5
        elif aux_shanghai_ranking_position < 200:
            scaled_shanghai_ranking_position = 4
        elif aux_shanghai_ranking_position < 300:
            scaled_shanghai_ranking_position = 3
        elif aux_shanghai_ranking_position < 400:
            scaled_shanghai_ranking_position = 2
        elif aux_shanghai_ranking_position < 500:
            scaled_shanghai_ranking_position = 1
        else:
            scaled_shanghai_ranking_position = 1
        return scaled_shanghai_ranking_position

    @staticmethod
    def city_size_scaling(city_size):
        if city_size < 100:
            aux_city_size = 1
        elif city_size > 500:
            aux_city_size = 5
        else:
            aux_city_size = round(city_size / 10)
        return aux_city_size

    @staticmethod
    def boolean_scaling(has_coast):
        if has_coast:
            aux_has_coast = 5
        else:
            aux_has_coast = 1
        return aux_has_coast

    # Precio entre 1 y 20€/m2
    @staticmethod
    def rental_price_scaling(average_rental_price):
        punctuation = [5, 4, 3, 2, 1]
        if average_rental_price > 20:
            average_rental_price = 20
        elif average_rental_price < 1:
            average_rental_price = 1
        return punctuation[round(5 * average_rental_price / 20)]

    @staticmethod
    def cut_off_grade_scaling(cut_off_grade):
        if cut_off_grade < 6:
            aux_scaled_cut_off_grade = 1
        elif cut_off_grade < 8:
            aux_scaled_cut_off_grade = 2
        elif cut_off_grade < 10:
            aux_scaled_cut_off_grade = 3
        elif cut_off_grade < 12:
            aux_scaled_cut_off_grade = 4
        elif cut_off_grade < 14:
            aux_scaled_cut_off_grade = 5
        else:
            aux_scaled_cut_off_grade = 5
        return aux_scaled_cut_off_grade

    # ALGORITHM
    def relevance_calculation(self):
        fixed_punctuation = 3 * (self.__average_score_univ_opinions + self.__shanghai_ranking_position)
        final_scaled_city_size = self.__scaled_city_size * self.__parameter_relevance.get_city_size_importance()
        final_scaled_has_coast = self.__scaled_has_coast * self.__parameter_relevance.get_has_coast_importance()
        final_cultural_score = self.__cultural_score * self.__parameter_relevance.get_cultural_score_importance()
        final_gastronomic_score = self.__gastronomic_score * self.__parameter_relevance.get_gastronomic_score_importance()
        final_leisure_score = self.__leisure_score * self.__parameter_relevance.get_leisure_score_importance()
        final_job_offer_score = self.__job_offer_score * self.__parameter_relevance.get_job_offer_score_importance()
        final_scaled_average_rental_price = self.__scaled_average_rental_price * self.__parameter_relevance.get_average_rental_price_importance()
        final_scaled_cut_off_grade = self.__scaled_cut_off_grade * self.__parameter_relevance.get_cut_off_grade_importancee()

        total_punctuation = fixed_punctuation + final_scaled_city_size + final_scaled_has_coast + final_cultural_score \
                            + final_gastronomic_score + final_leisure_score + final_job_offer_score + final_scaled_average_rental_price \
                            + final_scaled_cut_off_grade

        return self.scale_punctuation(total_punctuation)

    def scale_punctuation(self, total_punctuation):
        max_fixed_punctuation = 3 * (5 + 5)
        max_scaled_city_size = 5 * self.__parameter_relevance.get_city_size_importance()
        max_scaled_has_coast = 5 * self.__parameter_relevance.get_has_coast_importance()
        max_cultural_score = 5 * self.__parameter_relevance.get_cultural_score_importance()
        max_gastronomic_score = 5 * self.__parameter_relevance.get_gastronomic_score_importance()
        max_leisure_score = 5 * self.__parameter_relevance.get_leisure_score_importance()
        max_job_offer_score = 5 * self.__parameter_relevance.get_job_offer_score_importance()
        max_scaled_average_rental_price = 5 * self.__parameter_relevance.get_average_rental_price_importance()
        max_scaled_cut_off_grade = 5 * self.__parameter_relevance.get_cut_off_grade_importancee()

        max_total_punctuation = max_fixed_punctuation + max_scaled_city_size + max_scaled_has_coast + max_cultural_score \
                                + max_gastronomic_score + max_leisure_score + max_job_offer_score \
                                + max_scaled_average_rental_price + max_scaled_cut_off_grade

        return (total_punctuation / max_total_punctuation) * 10

    def get_average_score_univ_opinions(self):
        return self.__average_score_univ_opinions

    def get_shanghai_ranking_position(self):
        return self.__shanghai_ranking_position

    def get_scaled_city_size(self):
        return self.__scaled_city_size

    def get_scaled_has_coast(self):
        return self.__scaled_has_coast

    def get_cultural_score(self):
        return self.__cultural_score

    def get_gastronomic_score(self):
        return self.__gastronomic_score

    def get_leisure_score(self):
        return self.__leisure_score

    def get_job_offer_score(self):
        return self.__job_offer_score

    def get_scaled_average_rental_price(self):
        return self.__scaled_average_rental_price

    def get_scaled_cut_off_grade(self):
        return self.__scaled_cut_off_grade